#!/bin/sh
# Switch to the dir of this script
cd "$(dirname "$0")";
CWD="$(pwd)"
echo $CWD
# Wait 30 seconds unitl networking is functional
sleep 30
# Switch to virtual enviroment, than execute the bot and write log and errors to files
eval "source mensaEnviroment/bin/activate ; python mensaBot.py 1> log.out 2> err.out"

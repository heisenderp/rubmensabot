from lxml import html
import requests

q_west_url = "https://q-we.st/"

def getQWest(currDay):
    page = requests.get(q_west_url)
    tree = html.fromstring(page.content)
    title = tree.xpath("//div[@id='live_speiseplan_day_" + str(currDay)  + "']//span[@class='live_speiseplan_title']/text()")
    titles = tree.xpath("//div[@id='live_speiseplan_day_" + str(currDay)  + "']//span[@class='live_speiseplan_item_title']/text()")[1:]
    kennzeichen = tree.xpath("//div[@id='live_speiseplan_day_" + str(currDay)  + "']//sup[@class='live_speiseplan_item_kennzeichen']/text()")
    prices = tree.xpath("//div[@id='live_speiseplan_day_" + str(currDay)  + "']//span[@class='live_speiseplan_item_price']/text()")[1:]
    string = title[0] + '\n'
    for tit, kenn, price in zip(titles, kennzeichen, prices):
        string += '\t+ ' + tit + '\n\t\t(' + kenn + ') ' + price + '\n'        
    return string
